const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		// name
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'Error': 'name is required!'
			})
		}

		if(typeof(req.body.name)!=='string'){
			return res.status(400).send({
				'Error': 'name should be a string!'
			})
		}

		if(req.body.name==''){
			return res.status(400).send({
				'Error': 'name shouldnot be empty!'
			})
		}

		//ex
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'Error': 'ex is required!'
			})
		}

		if(typeof(req.body.ex)!=='object'){
			return res.status(400).send({
				'Error': 'ex should be an object!'
			})
		}

		if(req.body.ex==''){
			return res.status(400).send({
				'Error': 'ex should not be empty!'
			})
		}

		//alias
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'Error': 'alias is required!'
			})
		}

		if(typeof(req.body.alias)!=='string'){
			return res.status(400).send({
				'Error': 'alias should be a string!'
			})
		}

		if(req.body.alias==''){
			return res.status(400).send({
				'Error': 'alias should not be empty!'
			})
		}

		// 11
		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')) {
			if(exchangeRates.hasOwnProperty(req.body.alias)){
				return res.status(400).send({
					'Error': 'alias duplicate found!'
				})
			}
		}

		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')) {
			if(!exchangeRates.hasOwnProperty(req.body.alias)){
				return res.status(200).send({
					'Error': 'alias no duplicate found!'
				})
			}
		}






		
		return res.status(200).send({
			'Message': 'post /currency route is running'
		})
		
		

	
	})
}
