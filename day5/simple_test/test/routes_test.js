const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	//add solutions here
	it('test_api_post_currency', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200)
			done()
		})
	})

	//name
	it('test_api_post_nameMissing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'notName': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_nameNotString', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 123,
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_nameNotString', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': '',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	//ex
	it('test_api_post_exMissing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'notEx': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_exNotObject', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': 1
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_exEmpty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	//alias
	it('test_api_post_aliasMissing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'notalias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_aliasNotString', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 123,
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_nameNotEmpty', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': '',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	//11
	it('test_api_post_aliasDuplicate', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'usd',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})


	it('test_api_post_aliasDuplicate', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'wrongAlias',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200)
			done()
		})
	})


	




})
